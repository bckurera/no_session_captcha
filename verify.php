<?php
require_once 'dataBaseConnection.php';

if(isset($_POST['submit'])) 
 {
 	$enteredCode=trim($_POST['code']);
 	$referenceCode=trim($_POST['c_id']);

    $dataSet = dataBaseConnection::getCode($referenceCode);
    $timeTaken = time() - $dataSet['pvt_created_date'];
    if($timeTaken>$dataSet['pvt_life_time'] || $dataSet['pvt_captcha_status']!='CREATED')
    {
    	die('expired code');
    }
    
    //var_dump($referenceCode); die($enteredCode);
	if($enteredCode==$dataSet['str_verification_code'])
	{
		dataBaseConnection::updateCode($referenceCode,'VERIFIED');
		echo 'verified';//this is a human
		//Process the input as this is a legit request
	}else{
		dataBaseConnection::updateCode($referenceCode);
		echo 'not verified';//most probably not a human
	}  
}
?>