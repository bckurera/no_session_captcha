<?php
//login credentials to the database
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "db_test";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

// sql to create table
$sql = "CREATE TABLE tbl_captchas (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
str_referenceCode VARCHAR(128) NOT NULL,
str_verification_code VARCHAR(10) NULL,
pvt_created_date int(10),
pvt_captcha_status varchar(10),
pvt_life_time int(5) DEFAULT 300
)";

if ($conn->query($sql) === TRUE) {
    echo "Table tbl_captchas created successfully";
} else {
    echo "Error creating table: " . $conn->error;
}

$conn->close();
?>