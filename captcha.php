<?php


$string = '';
$refCode = '';

if(isset($_GET['ref']))
{
	$refCode = $_GET['ref'];
}else{
	die('<error>NO REF CODE FOUND !</error>');
}
 
for ($i = 0; $i < 8; $i++) 
{
	// this numbers refer to numbers of the ascii table (lower case)
	$string .= chr(rand(97, 122));
}

$dir = 'fonts/';
 
$image = imagecreatetruecolor(170, 60);
$black = imagecolorallocate($image, 0, 0, 0);
$color = imagecolorallocate($image, 10, 10, 10); // red
$invColor = imagecolorallocate($image, 200, 200, 200); // invisible_ink
$white = imagecolorallocate($image, 255, 255, 255);
 
imagefilledrectangle($image,0,0,399,99,$white);
for($i=0; $i<100; $i++)
{
	imagettftext ($image, 20, rand(0,10), 0, $i*10, $invColor, $dir."ts.ttf", 'xxxxxxxxxxxxxxxxxxxxxxxxx');
}

imagettftext ($image, 30, 2, 10, 40, $color, $dir."ts.ttf", $string);

header("Content-type: image/png");
imagepng($image);

require_once 'dataBaseConnection.php';
dataBaseConnection::addVerificationCode($refCode, $string); //store the reference and the code in the database
?>