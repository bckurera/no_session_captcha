# no_session_captcha
NO Session CAPTCHA implementation

Released under GPL V3

Reference:
https://ebckurera.wordpress.com/2017/04/24/captcha_with_no_session
