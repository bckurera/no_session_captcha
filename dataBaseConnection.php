<?PHP
class dataBaseConnection
{
	public static function addVerificationCode($refCode, $string)
	{
		
		$connObj = dataBaseConnection::establishDBconnection();

		$sql = "UPDATE tbl_captchas SET str_verification_code='".$string."' WHERE str_referenceCode ='".$refCode."';";

		if ($connObj->query($sql) != TRUE) 
		{
		    echo "Error updating record: " . $connObj->error;
		}

		$connObj->close();
	}

	public static function registerReference($captchaId)
	{
		$connObj = dataBaseConnection::establishDBconnection();

		$sql = 'INSERT INTO tbl_captchas (str_referenceCode, pvt_created_date, pvt_captcha_status)
		VALUES ("'.$captchaId.'",'.time().',"CREATED");'; 

		if (!mysqli_query($connObj, $sql)) {
		    echo "Error: " . $sql . "<br>" . mysqli_error($connObj);
		}

		mysqli_close($connObj);
	}

	public static function getCode($captchaId)
	{
		$connObj = dataBaseConnection::establishDBconnection();

		$sql = "SELECT * FROM tbl_captchas where str_referenceCode='".$captchaId."';";
		$result = $connObj->query($sql);

		$connObj->close();

		if ($result->num_rows ==1 ) {
		    // output data of each row
		    $row = $result->fetch_assoc();
		    return $row;
		}else{
			return '<error>NO OR MORE RECORDS</error>';
		}
	}

	public static function updateCode($captchaId,$status='USED')
	{
		$connObj = dataBaseConnection::establishDBconnection();

		$sql = "UPDATE tbl_captchas SET pvt_captcha_status='".$status."' WHERE str_referenceCode ='".$captchaId."';";

		if ($connObj->query($sql) != TRUE) 
		{
		    echo "Error updating record: " . $connObj->error;
		}

		$connObj->close();
	}

	private static function establishDBconnection()
	{
		//login credentials to the database
		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "db_test";

		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		};
		return $conn;
	}
 
}
?>





